#!/bin/sh

# print errors to the screen if ANYTHING goes wrong
set -e

# replace env vars
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# disable running as daemon in the background because of docker
nginx -g 'daemon off;'